#### Prerequisites
- terraform
- ansible
- an aws account


#### useful commands
set up EC2 instance
```bash
cd terraform
terraform apply --var-file staging.tfvars
```

set up https
```bash
cd ansible
ansible-playbook -i "locals/hosts.yml" -e "@locals/staging.yml" setup-instance.yml
ansible-playbook -i "locals/hosts.yml" -e "@locals/staging.yml" install-nginx.yml
```

login
```
ssh -i ./ssh_keys/key.pem ubuntu@34.244.37.88.sslip.io
```

chown to allow rsync to work
```
chown -R ubuntu /var/www/34.244.37.88.sslip.io
```

tear down all
```bash
cd terraform
terraform destroy --var-file staging.tfvars
```
