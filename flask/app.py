from flask import Flask, render_template
import hashlib

app = Flask(__name__)


# creates the gravatar hash for an email address
def create_gravatar_hash(email: str) -> str:
    return hashlib.md5(email.lower().encode('utf-8')).hexdigest()


@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html')


@app.route('/gravatar/<email>', methods=['GET'])
def get_gravatar_url(email):
    return "https://gravatar.com/avatar/{}".format(create_gravatar_hash(email))


if __name__ == '__main__':
    app.run(host = '0.0.0.0')
