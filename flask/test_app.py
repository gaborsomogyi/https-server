import pytest
from app import app, create_gravatar_hash


@pytest.fixture
def client():
    with app.test_client() as client:
        yield client


def test_empty_query(client):
    rv = client.get('/')
    assert b'hello flask' in rv.data


def test_gravatar_hash():
    test_email = r'info@example.com'
    assert create_gravatar_hash(test_email) == r'cb3045d1eb66dda5eae9ae2f96edeee9'


def test_gravatar_url(client):
    test_email = r'info@example.com'
    rv = client.get("/gravatar/{}".format(test_email))
    assert rv.data == b'https://gravatar.com/avatar/cb3045d1eb66dda5eae9ae2f96edeee9'
