style code with
```bash
autopep8 -i -r .
```

check coding style with
```bash
flake8
```
