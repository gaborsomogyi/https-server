variable "app_name" {
  description = "Name of the app"
  type        = string
}

variable "stage" {
  description = "Stage of deployment"
  type        = string
}

variable "instance_type" {
  description = "Instance type of the web EC2"
  type        = string
}

variable "subnet_id" {
  description = "The subnet the instance belongs to"
  type        = string
}

variable "security_group_ids" {
  description = "Security groups the instance belongs to"
  type        = list(string)
}

variable "ssh_public_key" {
  description = "SSH public key"
  type        = string
}

variable "eip_id" {
  description = "elastic ip allocation id"
  type        = string
}

variable "internet_gateway" {
  description = "link to the internet gateway"
}
