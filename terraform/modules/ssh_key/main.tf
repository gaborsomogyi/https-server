resource "tls_private_key" "generated_key" {
  # count     = var.ssh_public_key == null ? 1 : 0
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "keyfile" {
  sensitive_content = tls_private_key.generated_key.private_key_pem
  filename          = "${path.root}/../shared/private/key.pem"
  file_permission   = "0400"
}
