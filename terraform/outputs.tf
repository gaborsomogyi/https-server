output "public_ip" {
  value       = module.web.public_ip
  description = "The public ip of the web instance"
}
