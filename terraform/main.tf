terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
  required_version = ">= 0.14"
}

/*
terraform {
  backend "s3" {
    bucket = "terraform-flask-ec2"
    key    = "https-server"
    region = "eu-west-1"
  }
}
*/

provider "aws" {
  profile = "default"
  region  = var.region
}

module "web" {
  source = "./modules/web"

  instance_type      = var.instance_type
  subnet_id          = module.vpc.subnet_id
  security_group_ids = module.vpc.security_group_ids
  ssh_public_key     = var.ssh_public_key != null ? var.ssh_public_key : module.ssh_key[0].public_key # tls_private_key.generated_key[0].public_key_openssh

  eip_id           = var.eip_id
  internet_gateway = module.vpc.internet_gateway

  app_name = var.app_name
  stage    = var.stage
}

module "vpc" {
  source = "./modules/vpc"

  availability_zone = "${var.region}a"

  app_name = var.app_name
  stage    = var.stage
}

module "ssh_key" {
  source = "./modules/ssh_key"
  count  = var.ssh_public_key == null ? 1 : 0
}

resource "local_file" "hosts" {
  content  = module.web.public_ip
  filename = "${path.root}/../shared/private/hosts"
}
resource "local_file" "vars" {
  content  = "---\ndomain_name: ${module.web.public_ip}.sslip.io"
  filename = "${path.root}/../shared/private/vars.yml"
}
