variable "region" {
  description = "specifies the AWS region"
  default     = "eu-west-1"
}

variable "ssh_public_key" {
  description = "SSH public key"
  default     = null
}

variable "app_name" {
  description = "Name of the app"
  type        = string
  default     = "app"
}

variable "stage" {
  description = "Stage of deployment"
  type        = string
  default     = "staging"
}

variable "instance_type" {
  description = "instance type for the web ec2"
  type        = string
  default     = "t2.micro"
}

variable "eip_id" {
  type        = string
  description = "elastic ip allocation"
  default     = null
}
