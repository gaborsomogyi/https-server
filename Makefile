setup: terraform-init terraform-apply ansible-setup
destroy: terraform-destroy

static: ansible-static
flask: ansible-flask

terraform-init:
	(cd terraform && terraform init)
terraform-apply:
	(cd terraform && terraform apply -auto-approve)
terraform-destroy:
	(cd terraform && terraform destroy -auto-approve)

ansible-setup:
	(cd ansible && ansible-playbook setup-instance.yml)
ansible-static:
	(cd ansible && ansible-playbook static.yml)
ansible-flask:
	(cd ansible && ansible-playbook flask.yml)
