Local settings can be put here in a yml file

example usage: `ansible-playbook -e "@locals/staging.yml" setup-instance.yml`


Also hosts can be put here:

example usage: `ansible-playbook -i "locals/hosts.yml" setup-instance.yml`


Both of them: `ansible-playbook -i "locals/hosts.yml" -e "@locals/staging.yml" setup-instance.yml`
