pid /run/nginx.pid;
user www-data;

events {
	worker_connections 4096;
}

http {
	types {
	  text/html                             html htm shtml;
	  text/css                              css;
	  text/xml                              xml rss;
	  image/gif                             gif;
	  image/jpeg                            jpeg jpg;
	  application/x-javascript              js;
	  text/plain                            txt;
	  image/png                             png;
	  image/x-icon                          ico;
	  application/zip                       zip;
	}

    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    # we make sure the server only works with the correct domain name
	server {
		server_name _;
		listen *:80 default_server deferred;
		return 444;
	}

	# to allow certbot
	server {
		location ^~ /.well-known/acme-challenge/ {
			default_type "text/plain";
			alias /var/www/acme-challenge/;
		}
	}

	server {
		listen 80;
		server_name {{ domain_name }} *.{{ domain_name }};

		location / {
			return 301 https://{{ domain_name }}$request_uri;
		}
	}

	server {
		listen 443 ssl;
		server_name  {{ domain_name }};

		ssl_certificate /etc/letsencrypt/live/{{ domain_name }}/fullchain.pem;
		ssl_certificate_key /etc/letsencrypt/live/{{ domain_name }}/privkey.pem;
		include /etc/letsencrypt/options-ssl-nginx.conf;
		ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

		include /etc/nginx/app.conf;
	}

	# for additional server configs
	include /etc/nginx/conf.d/*;


}
